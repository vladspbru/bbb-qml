#include "bbb_plugin.h"
#include "net.h"
#include "bbbdevice.h"
#include "bbbdevices.h"
#include "bbbdatasource.h"
#include "xyprovider.h"

#include <qqml.h>
#include <QtQml>

// Second, define the singleton type provider function (callback).
static QObject* NetConnection_singletontype_provider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    qDebug() << Q_FUNC_INFO;

    engine->setObjectOwnership(&DTGNET, QQmlEngine::CppOwnership);
    return &DTGNET;
}

static QObject* BbbDevices_singletontype_provider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    qDebug() << Q_FUNC_INFO;

    engine->setObjectOwnership(&BBBs, QQmlEngine::CppOwnership);
    return &BBBs;
}

void BBBPlugin::registerTypes(const char *uri)
{
    // @uri org.gt.vxcomponents
    qDebug() << Q_FUNC_INFO << " uri="<< uri;
    qmlRegisterSingletonType<DtgNet>(uri, 1, 0, "GtNet", NetConnection_singletontype_provider);
    qmlRegisterSingletonType<BBBDevices>(uri, 1, 0, "BBBs", BbbDevices_singletontype_provider);

    qmlRegisterType<BBBDevice>(uri, 1, 1, "BBBDevice");
    qmlRegisterType<BBBDataSource>(uri, 1, 1, "BBBDataSource");
    qmlRegisterType<XYProvider>(uri, 1, 1, "XYProvider");

}

