#ifndef MYPROP_HELPERS_H
#define MYPROP_HELPERS_H

#include <QObject>

#define MYSIMPLE_PROP(pType, pName) \
    Q_PROPERTY(pType pName READ pName WRITE pName NOTIFY pName##Changed) \
    public: void pName(const pType& pVal) { if( pName##_ != pVal ) { pName##_ = pVal; Q_EMIT pName##Changed();} }\
    public: pType pName() const { return pName##_; } \
    private: pType pName##_; \
    Q_SIGNALS: void pName##Changed(); \
    private:



//MYPROP(int, testProp2, "")




#endif // MYPROP_HELPERS_H
