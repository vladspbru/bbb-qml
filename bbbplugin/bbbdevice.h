#ifndef BBBDEVICE_H
#define BBBDEVICE_H

//#include "bbb_pch.h"
#include "uart_packet.h"
#include "dmpacket2.h"
#include "myprop_helpers.h"

class BBBDevice : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(BBBDevice)

    MYSIMPLE_PROP(int, addr)
    MYSIMPLE_PROP(QString, name)
    MYSIMPLE_PROP(QString, ipaddr)
    MYSIMPLE_PROP(bool, alive)

    Q_PROPERTY( unsigned int count READ count NOTIFY dataChanged )
    Q_PROPERTY( unsigned int ms READ ms NOTIFY dataChanged )
    Q_PROPERTY( int conf READ conf NOTIFY iChanged )
    Q_PROPERTY( int sigs READ sigs NOTIFY iChanged )
    Q_PROPERTY( int out  READ out  NOTIFY iChanged )
    Q_PROPERTY( int f READ f NOTIFY fChanged )
    Q_PROPERTY( double v READ v NOTIFY fChanged )
    Q_PROPERTY( double a READ a NOTIFY fChanged )
    Q_PROPERTY( double vz READ vz NOTIFY dataChanged )

public:
    explicit BBBDevice( QObject *parent = nullptr );
    ~BBBDevice();

    unsigned int count() const   { return data.count; }
    unsigned int ms() const   { return data.ms; }
    int conf() const             { return data.conf; }
    int sigs() const             { return data.sigs; }
    int out() const              { return data.tp_out; }
    int f() const                { return data.F; }
    double a() const             { return data.A/10.; }
    double v() const             { return data.block8.w1_V==0 ? 0 : 1000000/7/data.block8.w1_V/10.; }
    double vz() const            { return data.block8.w3_V2==0 ? 0 : 1000000/7/data.block8.w3_V2/10.; }

    //вспомогательная расшифровка
    int rrs() const  { return (data.sigs & 0x01)? 1:0; }
    int rc() const   { return (data.sigs & 0x02)? 1:0; }
    int _rc() const  { return (data.sigs & 0x04)? 1:0; }
    qreal tp() const   {
        switch(data.tp_out){
            case 1: return 1.;
            case 2: return 2.;
            case 4: return 3.;
            case 8: return 4.;
            case 3: return 0.5;
            case 5: return 1.5;
            case 6: return 2.5;
            case 7: return 3.5;
            case 0: return 0;
            case 32: return -1;
            default: return data.tp_out;
        }
    }

Q_SIGNALS:
    void dataChanged();
    void fChanged();
    void iChanged();

public Q_SLOTS:
    void loadDtg(const TDatagram2& dtg, const QString& saddr);

    Q_INVOKABLE void sendQuery(const QString &toaddr);

private:
    unsigned int last_count;
    void timerEvent(QTimerEvent *);

private:
    tSoftOut data;
};

#endif // BBBDEVICE_H
