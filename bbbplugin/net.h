#ifndef Net_H
#define Net_H

#include "gtlan.h"
#include "vsingleton.h"
#include "gtlan_mcast_const.h"

class GtLan;
class DtgNet : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(DtgNet) // OK - copy constructor and assignment operators
                             // are now disabled, so the compiler won't implicitely
                             // generate them.

    Q_PROPERTY( bool enabled READ enabled WRITE set_enabled NOTIFY enabledChanged)
//    Q_PROPERTY( int count READ enabled WRITE set_enabled NOTIFY enabledChanged)

    QScopedPointer<GtLan> dtg_net;
    bool enabled_;

public:

    explicit DtgNet(QObject *parent = nullptr, bool open=true);
    void Open();

    bool enabled() const {  return enabled_;  }

public Q_SLOTS:
    void set_enabled(bool e);
    void StartStop(void) {
        if ( enabled_ )
            set_enabled( false );
        else set_enabled( true );
    }

    size_t send(const QString& s) {
        return dtg_net->send( s.toUtf8() );
    }

    size_t send(const TDatagram2& dtg) {
        return dtg_net->send( QByteArray( (const char*)&dtg, dtg.Size + TDatagram2_HEADER_LN ) );
    }

    size_t sendto(const QByteArray& ba, QHostAddress addr, quint16 port){
        return dtg_net->sendto( ba, addr, port );
    }

    Q_INVOKABLE void dtgRecived(const QByteArray& d, const QHostAddress& addr);


Q_SIGNALS:
    void enabledChanged();

//    void onString(const QString& s, const QString& saddr);
    void onDtg(const TDatagram2& dtg, const QString& saddr);


private:
    friend class Singleton<DtgNet>;
};

#define DTGNET Singleton<DtgNet>::instance()


#endif // Net_H
