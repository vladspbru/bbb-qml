#include "bbbdevices.h"
#include <QtQml>


BBBDevices::BBBDevices(QObject *parent)
    : QObject(parent)
    , devs()
{
    devs.reserve(64);
}

BBBDevices::~BBBDevices()
{
}

uint BBBDevices::count() const
{
    return devs.size();
}

void BBBDevices::setCount(uint c)
{
    devs.resize(c);
    dss.resize(c);
    Q_EMIT countChanged();
}

BBBDevice*  BBBDevices::at(int idx )
{
    if(idx < 0) return nullptr;

    if( idx >= devs.size() )
        setCount(idx+1);

    dev_ptr &d = devs.at(idx);
    if(!d){
        d = new BBBDevice(this) ;
        d->addr(idx);
        d->name( QString("A-")+QString::number(idx) );
        QQmlEngine::setObjectOwnership(d, QQmlEngine::CppOwnership);
    }

    return d;
}

BBBDataSource*  BBBDevices::datasource(int idx)
{
    ds_ptr &ds=dss.at(idx);
    if(!ds){
        dev_ptr bbb = at( idx );
        if(!bbb) return nullptr;
        ds = new BBBDataSource(this);
        ds->setDevice(bbb);
        QQmlEngine::setObjectOwnership(ds, QQmlEngine::CppOwnership);
    }

    return ds;
}

