#include "gtlan.h"


GtLan::GtLan(QObject *parent) :
    QObject(parent)
  ,isocket_(nullptr)
  ,idat_(2048, 0)
  ,osocket_(nullptr)
  ,odat_(2048, 0)
{
}

GtLan::~GtLan()
{
    close();
}

void GtLan::on_error(QAbstractSocket::SocketError err)
{
    qWarning() << "GtLan::ERROR: " << err << endl;
}

void GtLan::close()
{
    if (is_open_) {
        is_open_ = false;
        osocket_->close();
        isocket_->close();
    }
}

int GtLan::open(QHostAddress groupAddress, quint16 groupPort, QHostAddress localhostAddress)
{
    multicast_address_ = groupAddress;
    multicast_port_ = groupPort;
    localhost_address_ = localhostAddress;

    if(localhost_address_ != QHostAddress::AnyIPv4){
        QList<QNetworkInterface> list  = QNetworkInterface::allInterfaces();
        foreach (auto i, list) {
            if( i.hardwareAddress() == localhost_address_.toString() ){
                intrf = i;
                break;
            }
        }
    }

    //sender -----------------------------------------------
    osocket_.reset( new QUdpSocket(this) );
    connect(osocket_.get(), SIGNAL(error(QAbstractSocket::SocketError)),
            this,     SLOT(on_error(QAbstractSocket::SocketError)) );


    if ( ! osocket_->bind(localhost_address_, 0, QUdpSocket::ReuseAddressHint | QUdpSocket::ShareAddress) )
        return -1;
    if( intrf.isValid() )
        osocket_->setMulticastInterface(intrf);
    //receiver -----------------------------------------------
    isocket_.reset( new QUdpSocket(this) );
    connect(isocket_.get(), SIGNAL(error(QAbstractSocket::SocketError)),
            this,     SLOT(on_error(QAbstractSocket::SocketError)) );
    connect(isocket_.get(), SIGNAL(readyRead()),
            this, SLOT(readData()));

    if ( ! isocket_->bind(localhost_address_, groupPort, QUdpSocket::ReuseAddressHint | QUdpSocket::ShareAddress) )
        return -1;
    else {
        isocket_->joinMulticastGroup(multicast_address_);
        if( intrf.isValid() )
            isocket_->setMulticastInterface(intrf);
    }

    is_open_ = true;
    return 0;
}

void GtLan::readData()
{
//    qDebug() << Q_FUNC_INFO;
    quint16 senderPort;
    while (isocket_->hasPendingDatagrams()) {
        idat_.resize(isocket_->pendingDatagramSize());
        isocket_->readDatagram(idat_.data(), idat_.size(), &sender_address_, &senderPort);
        Q_EMIT dataRecived(idat_, sender_address_);
    }
}


size_t GtLan::sendto(const QByteArray& d, QHostAddress addr, quint16 port)
{
//    qDebug() << Q_FUNC_INFO;
    odat_ = d;
    return osocket_->writeDatagram( odat_.data(), odat_.size(), addr, port );
}
