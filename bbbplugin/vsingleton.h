#ifndef VSINGLETON_H
#define VSINGLETON_H
//---------------------------------------------------------------------------
//  Copyright 2015. All Rights Reserved.
//  Author:   Vlad Zverev
//----------------------------------------------------------------------------


template <class TYPE>
class Singleton
{
public:
	static TYPE& instance(void) {
		static TYPE singleton;
		return singleton;
	}

private:
	Singleton();
	~Singleton();
	Singleton(const Singleton &);
	Singleton& operator=(const Singleton &);
};


//---------------------------------------------------------------------------
#endif /* VSINGLETON_H */
