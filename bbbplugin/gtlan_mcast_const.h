#ifndef GTLAN_MCAST_CONST_H
#define GTLAN_MCAST_CONST_H
//----------------------------------------------------------------------------



#define GTLAN_MCAST_ADDR "224.168.123.4"    //network addr
#define GTLAN_MCAST_PORT 12347              //base port

#define GTLAN_MCAST_PORT4SHARE   GTLAN_MCAST_PORT+1  //gt_share
#define GTLAN_MCAST_PORT4MESNET  GTLAN_MCAST_PORT+2  //mesnet_u.dll

#define GTLAN_PORT4BBB_DIAGN    GTLAN_MCAST_PORT+3  //bbb diagn string port
#define GTLAN_PORT4BBB_COMMAND  GTLAN_MCAST_PORT+4  //bbb command input port






//---------------------------------------------------------------------------
#endif
