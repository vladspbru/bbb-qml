#include "xyprovider.h"
#include <QtCharts/QXYSeries>
#include <QtCharts/QAreaSeries>
#include <QtCharts/QValueAxis>
#include <QtCore/QDebug>
#include <QMutexLocker>
#include <algorithm>
#include <limits>

using namespace QtCharts;


XYProvider::XYProvider(QObject *parent)
    : QObject(parent)
    ,ds_(nullptr)
    ,regimX_(0)
    ,last_tick_(0)
{

}

XYProvider::~XYProvider()
{

}

void XYProvider::setDatasource(BBBDataSource* ds)
{
    if(ds_)
        ds_->disconnect(this);

    ds_ = ds;

    if(ds_)
        QObject::connect(ds_, &BBBDataSource::tickChanged,
                         this, &XYProvider::dataChanged );

    Q_EMIT datasourceChanged();
    updateSeries();
}

void XYProvider::setRegimX(int r)
{
    if( regimX_ != r ){
        regimX_ = r;
        updateSeries();
    }
}


QPointF XYProvider::generate_point(const BBBDataSource::data_type& d)
{
    qreal x=-1;
    switch (regimX_) {
    default:
    case 0 : x=d.id;
        break;
    case 1:  x=d.syntetic_tm;
        break;
    case 2:  x=d.ms/1000.;
        break;
    }

    qreal y=-1;
    switch (paramY_) {
    case 0: y=d.rc;
        break;
    case 1: y=d.tp;
        break;
    case 2: y=d.v;
        break;
    case 3: y=d.vz;
        break;
    case 4: y=d.f;
        break;
    case 5: y=d.a;
        break;
    case 6: y=d.ms;
        break;
    case 7: y=d._rc;
        break;
    default: y=-1;
    }
    return QPointF(x,y);
}

void XYProvider::updateFromQML( QAbstractSeries *series )
{
    if (!series || !ds_ ) return;
    //    qDebug() << Q_FUNC_INFO;

    QXYSeries  *xySeries = static_cast<QXYSeries *>(series);
    auto dset = ds_->get_datas();
    QMutexLocker lock(dset.second);
    BBBDataSource::container_type *datas = dset.first;

    if( datas->size()<2 ){ //нету данных рисовать нечего
        xySeries->clear();
        return;
    }

    if( last_tick_==0 ) //если полное обновление, при смене режима например
        xySeries->clear();

    if( last_tick_ == datas->last().id )// если повтор
        return;

    BBBDataSource::container_type::const_reverse_iterator f =
            std::find_if(datas->rbegin(), datas->rend(),
                         [this](const BBBDataSource::data_type& d){ return d.id==this->last_tick_; }
    );

    QPointF min(minX(), minY());
    QPointF max(maxX(), maxY());
    if( xySeries->count() < 2 ){//если начинаем заново строить графики
        min.setX( std::numeric_limits<qreal>::max() );
        min.setY( std::numeric_limits<qreal>::max() );
        max.setX( std::numeric_limits<qreal>::min() );
        max.setY( std::numeric_limits<qreal>::min() );
    }
    QList<QPointF> points;
    BBBDataSource::container_type::const_reverse_iterator i = f;
    do {
        --i;
//        qDebug() << Q_FUNC_INFO<<xySeries->name()<<(*i).id;
        QPointF p = generate_point(*i);
        points.append( p );
        min.setX( std::min( p.x(), min.x()) );
        min.setY( std::min( p.y(), min.y()) );
        max.setX( std::max( p.x(), max.x()) );
        max.setY( std::max( p.y(), max.y()) );
    }
    while(i != datas->rbegin() );

    if( f==datas->rend() ) {//ненашли, все перерисоввываем
        xySeries->replace(points);
        qDebug() << Q_FUNC_INFO<<xySeries->name()<<points.size();
    }
    else
        xySeries->append(points);
    minX( min.x() );
    minY( min.y() );
    maxX( max.x() );
    maxY( max.y() );
    last_tick_ = datas->last().id;
}

