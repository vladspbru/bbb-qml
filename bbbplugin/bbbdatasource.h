#ifndef BBBDATASOURCE_H
#define BBBDATASOURCE_H

#include <QtCore/QObject>
#include "uart_packet.h"
#include "myprop_helpers.h"
#include "bbbdevice.h"
#include <QMutex>

class BBBDevice;

class BBBDataSource : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(BBBDataSource)

    MYSIMPLE_PROP(int,  max_points)
    MYSIMPLE_PROP(uint, tick)
    MYSIMPLE_PROP(uint, count)

    Q_PROPERTY( BBBDevice* device  READ device WRITE setDevice  NOTIFY deviceChanged )


public:
    explicit BBBDataSource(QObject *parent = nullptr);
    ~BBBDataSource();

    struct data_type {
        int   addr;
        uint  ms;
        int   rrs;
        int   rc;
        int   _rc;//предварительная рц
        qreal tp; //команды ТП торможения
        int   f; //частота Гц
        qreal a; //ускорение торможения
        qreal v; //измеряемая скорость
        qreal vz;
        uint  id; //счетчик порядковый точек
        qreal syntetic_tm; //синтетическое время в секундах с начала работы
    };

    typedef QList<data_type> container_type;
    std::pair< container_type*, QMutex* > get_datas() { return std::make_pair(&data_, &mtx_); }

    BBBDevice* device() { return dev_; }
    void setDevice(BBBDevice* dev);
    void load_data();
    void addPoints(data_type &p);

Q_SIGNALS:
    void deviceChanged();

public Q_SLOTS:
    Q_INVOKABLE void save_captured(const QString &filename="");
    Q_INVOKABLE void clear();


private:

    BBBDevice*  dev_;
    container_type   data_;
    QMutex      mtx_;
    data_type   last_;
};

#endif // BBBDATASOURCE_H
