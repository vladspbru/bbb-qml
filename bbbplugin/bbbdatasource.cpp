#include "bbbdatasource.h"
#include "bbbdevice.h"
#include <QtCore/QDebug>
#include <QtCore/QtMath>
#include <QtCore/QRandomGenerator>
#include <QMutexLocker>
#include <limits>


BBBDataSource::BBBDataSource(QObject *parent):
    QObject(parent)
  ,tick_(0)
  ,count_(0)
  ,dev_(nullptr)
  ,mtx_(QMutex::Recursive)
  ,last_()
{
    max_points_ = 10000; //колво накапливаемых фреймов
    data_.push_back(last_);//чтобы не проверять размер при работе с последним значением
}

BBBDataSource::~BBBDataSource()
{
}

void BBBDataSource::setDevice(BBBDevice* d)
{
    if(dev_)
        dev_->disconnect(this);
    dev_ = d;
    if(dev_)
        QObject::connect(dev_, &BBBDevice::dataChanged,
                         this, &BBBDataSource::load_data);
    Q_EMIT deviceChanged();
}

bool compare_float(qreal f1, qreal f2, qreal precision = 0.0001)
{
    if (((f1 - precision) < f2) &&  ((f1 + precision) > f2))
        return true;
    else  return false;
}


void BBBDataSource::load_data()
{
    //    qDebug() << Q_FUNC_INFO;
    if( !max_points() || !dev_) return;

    //накапливаем данные
    data_type now;
    now.addr = dev_->addr();
    now.ms = dev_->ms();
    now.rrs = dev_->rrs();
    now.rc = dev_->rc();
    now._rc = dev_->_rc();
    now.tp = dev_->tp();
    now.f = dev_->f();
    now.a = dev_->a();
    now.v = dev_->v();
    now.vz = dev_->vz();
    now.id = 0;
    now.syntetic_tm = 0;

    //> todo remove this test
    //    now.f += now.f*QRandomGenerator::global()->generateDouble();

    //смотрим изменение в значимых/отслеживаемых областях
    bool changed = now.addr != last_.addr
            || now.rrs != last_.rrs
            || now.rc != last_.rc
            || now._rc != last_._rc
            || !compare_float(now.tp, last_.tp)
            || ( (now._rc || now.rc) && now.f != last_.f) //вырезаем по рц иначе много хлама
            || ( (now._rc || now.rc) && !compare_float(now.a, last_.a) )
            || !compare_float(now.vz, last_.vz)
            ;

    if(changed){ //если есть изменения
        QMutexLocker lock(&mtx_);
        //зафиксировано ли последнее состояние перед изменением
        if( /*data_.size() &&*/ data_.back().ms != last_.ms )
            addPoints(last_);
        //добавляем текущее
        addPoints(now);
        //чистим старые
        if( data_.size() > max_points() ){
            data_.erase( data_.begin(), data_.begin()+data_.size()/10 );
            count( data_.size() );
        }
    }
    last_ = now;
}

//интервал времени, при переходе через 0
uint distance(uint ie, uint ib)
{
    if(ie >= ib)
        return ie - ib;
    else{
        return std::numeric_limits<uint>::max() - ib + ie + 1;
    }
}

void BBBDataSource::addPoints(data_type& p)
{
    p.id = tick()+1;

    data_type& back = data_.back();
    uint dt = distance(p.ms, back.ms);
    const int IT = 3000; //если событие более 3 секунд, ограничиваем
    if(dt>IT)
        dt = IT;
    p.syntetic_tm = back.syntetic_tm + dt/1000.;

    data_.push_back(p);
    tick( p.id );
    count( data_.size() );
    //    qDebug() << Q_FUNC_INFO << tick();
}



#include <fstream>
void BBBDataSource::save_captured(const QString& filename)
{
    std::string fn = filename.toStdString();
    if (fn.empty()){
        fn = "bbb-data-captured.csv";
    }
    std::ofstream csv(fn, std::ofstream::out | std::ofstream::app);
    QMutexLocker lock(&mtx_);
    for(const data_type& i : data_){
        csv << std::dec << i.addr
            << ";" <<std::dec<< i.ms
            << ";" <<std::hex<< i.rrs
            << ";" <<std::hex<< i.rc
            << ";" <<std::hex<< i._rc
            << ";" <<std::fixed<<std::setprecision(1)<< i.tp
            << ";" <<std::dec<< i.f
            << ";" <<std::fixed<<std::setprecision(3)<< i.a
            << ";" <<std::fixed<<std::setprecision(2)<< i.v
            << ";" <<std::fixed<<std::setprecision(2)<< i.vz
            << ";" <<std::dec<< i.id
            << ";" <<std::fixed<<std::setprecision(3)<< i.syntetic_tm
            << std::endl;
    }
}

void BBBDataSource::clear()
{
    {
    QMutexLocker lock(&mtx_);
    data_.erase(data_.begin(), data_.end());
    data_.push_back(last_);
    }
//    tick( 0 );
    count( data_.size() );
}





