#ifndef GtLan_H
#define GtLan_H

#include <QObject>
#include <QtNetwork>
#include "dmpacket2.h"


class GtLan : public QObject
{
    Q_OBJECT

private:
    bool is_open_;

    QHostAddress multicast_address_;
    quint16      multicast_port_;
    QHostAddress localhost_address_;
    QHostAddress sender_address_;

    QScopedPointer<QUdpSocket> isocket_;
    QByteArray   idat_;
    QScopedPointer<QUdpSocket> osocket_;
    QByteArray  odat_;

    QNetworkInterface intrf;

private Q_SLOTS:
    void on_error(QAbstractSocket::SocketError);
    void readData();

Q_SIGNALS:
    void dataRecived(const QByteArray& d, const QHostAddress& addr);

public:

    explicit GtLan(QObject *parent = nullptr);
    ~GtLan();
    int open(QHostAddress groupAddress, quint16 groupPort, QHostAddress localhostAddress=QHostAddress::AnyIPv4);
    void close();
    bool is_open() const { return is_open_; }

    size_t sendto(const QByteArray& d, QHostAddress addr, quint16 port);

    size_t send(const QByteArray& d) {
        return sendto(d, multicast_address_, multicast_port_);
    }
};

#endif // GtLan_H
