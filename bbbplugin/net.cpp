#include "net.h"
#include "gtlan_mcast_const.h"

DtgNet::DtgNet(QObject *parent, bool open) : QObject(parent)
  ,dtg_net()
  ,enabled_(false)
{
    if(open)
        Open();
}

void DtgNet::set_enabled( bool e )
{
    if(enabled_ != e){
        enabled_ = e;

        if( !dtg_net->is_open() ){
            enabled_ = false;
            qWarning() << "NetConnection::dtg_net->is_open()==false";
        }

        if(enabled_){
            dtg_net->connect(dtg_net.data(), SIGNAL(dataRecived(const QByteArray&, const QHostAddress&)),
                             this, SLOT(dtgRecived(const QByteArray&, const QHostAddress&)));
        }
        else {
            dtg_net->disconnect(this);
        }
        Q_EMIT enabledChanged();
    }
}

void DtgNet::Open()
{
    qDebug() << Q_FUNC_INFO;
    dtg_net.reset( new GtLan(this) );

    const char* addr = getenv("GTLAN_MCAST_ADDR");
    if ( !addr ) addr = GTLAN_MCAST_ADDR;
    int port=0;
    const char* chport = getenv("GTLAN_MCAST_PORT");
    if ( chport ) port = atoi(chport);
    if ( !port ) port = GTLAN_MCAST_PORT;

    const char* chlocal_intf = getenv("GTLAN_MCAST_LOCAL_INTERFACE");

    bool ok = false;
    if ( chlocal_intf ){
        qDebug()<<"GTLAN_MCAST_LOCAL_INTERFACE: "<<chlocal_intf;
        ok = dtg_net->open(QHostAddress(addr), port, QHostAddress(chlocal_intf))==0;
    }
    else
        ok = dtg_net->open(QHostAddress(addr), port)==0;

    if( ok )
        set_enabled(true);
}

void DtgNet::dtgRecived(const QByteArray& d, const QHostAddress& addr)
{
//    qDebug() << Q_FUNC_INFO<<" -> "<<d.size()<<"b from"<< addr.toString();
    const TDatagram2* dtg = reinterpret_cast<const TDatagram2*> ( d.data() );
    Q_EMIT onDtg(*dtg, addr.toString());

//    qDebug() << "убрать код ";
//    static const QMetaMethod onStringSignal = QMetaMethod::fromSignal(&DtgNet::onString);
//    if( isSignalConnected(onStringSignal) ) {
//        QString str = QString::fromUtf8( d.data() );
//        Q_EMIT onString(str, addr.toString());
//    }
}

