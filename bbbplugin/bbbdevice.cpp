#include "bbbdevice.h"
#include "net.h"

BBBDevice::BBBDevice(QObject *parent):
    QObject(parent)
  , name_("BBB")
{
    connect(&DTGNET, SIGNAL(onDtg(const TDatagram2&, const QString&)),
            this, SLOT(loadDtg(const TDatagram2&, const QString&)));

    //    qDebug() << Q_FUNC_INFO;
    last_count = data.count;
    alive_ = false;
    startTimer(1000);   // millisecond timer
}

BBBDevice::~BBBDevice()
{
    qDebug() << Q_FUNC_INFO << name();
}

void BBBDevice::timerEvent(QTimerEvent *)
{
    //    qDebug() << Q_FUNC_INFO << ipaddr_ << "  " << data.count - last_count;
    bool is_alive = (last_count != data.count);
    last_count = data.count;
    alive(is_alive);
}

void BBBDevice::loadDtg(const TDatagram2& dtg, const QString &saddr)
{
    //    qDebug() << Q_FUNC_INFO;
    if( dtg.Type != 33)
        return;
    const tSoftOut* t = reinterpret_cast<const tSoftOut*>(&dtg.Data[0]);
    if( t->block8.b0_addr != addr_)
        return;

    ipaddr(saddr);

    int i1 = 0;
    if(data.conf != t->conf) i1++;
    if(data.sigs != t->sigs) i1++;
    if(data.tp_out != t->tp_out) i1++;

    int f1 = 0;
    if(data.F != t->F) f1++;

    data = *t;
    Q_EMIT dataChanged();

    if(i1)
        Q_EMIT iChanged();
    if(f1)
        Q_EMIT fChanged();

}

void BBBDevice::sendQuery(const QString& toaddr)
{
    QString ad = toaddr.isEmpty() ? ipaddr() : toaddr;
    if(ad.isEmpty())
            return;
//    qDebug() << Q_FUNC_INFO << " query address: " <<ad;

    static TDatagram2 dtg;
    tSoftIn  *qry = reinterpret_cast<tSoftIn*>(&dtg.Data[0]);
    qry->block8.b0_addr = uint8_t(addr_);

    dtg.Type = 34;
    dtg.setName("bbb");
    dtg.Size = sizeof(*qry);

    DTGNET.sendto( QByteArray( (const char*)&dtg, dtg.Size + TDatagram2_HEADER_LN )
                   , QHostAddress(ad), GTLAN_PORT4BBB_COMMAND );
}


