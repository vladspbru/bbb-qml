#ifndef BBB_PLUGIN_H
#define BBB_PLUGIN_H

#include <QQmlExtensionPlugin>

class BBBPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface/1.0")

public:
    void registerTypes(const char *uri) override;
};

#endif // BBB_PLUGIN_H
