#ifndef BBBDEVICES_H
#define BBBDEVICES_H

#include <QObject>
#include <vector>
#include <memory>
#include "bbbdevice.h"
#include "bbbdatasource.h"
#include "vsingleton.h"

class BBBDevice;
class BBBDataSource;

class BBBDevices : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(BBBDevices) // OK - copy constructor and assignment operators

    Q_PROPERTY( uint count READ count WRITE setCount NOTIFY countChanged)

    friend class Singleton<BBBDevices>;

public:
    explicit BBBDevices(QObject *parent = nullptr);
    ~BBBDevices();

    uint  count() const;
    void setCount(uint c);

    Q_INVOKABLE BBBDevice*  at(int idx );
    Q_INVOKABLE BBBDataSource*  datasource(int idx );

Q_SIGNALS:
    void countChanged();

public Q_SLOTS:

private:
//    typedef std::unique_ptr<BBBDevice> bbbdevice_ptr;
//    std::vector< bbbdevice_ptr > devs;

    typedef BBBDevice* dev_ptr;
    typedef BBBDataSource* ds_ptr;

    std::vector< dev_ptr > devs;
    std::vector< ds_ptr > dss;
};

#define BBBs Singleton<BBBDevices>::instance()


#endif // BBBDEVICES_H
