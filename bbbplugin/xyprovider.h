#ifndef XYPROVIDER_H
#define XYPROVIDER_H

#include <QObject>
#include <QtCore/QObject>
#include <QtCharts/QAbstractSeries>
#include <QtCharts/QAbstractAxis>
#include "myprop_helpers.h"
#include "bbbdatasource.h"
#include <utility>
#include <vector>
#include <QPointF>

class BBBDataSource;

class XYProvider : public QObject
{
    Q_OBJECT

    MYSIMPLE_PROP(qreal, minX)
    MYSIMPLE_PROP(qreal, maxX)
    MYSIMPLE_PROP(qreal, minY)
    MYSIMPLE_PROP(qreal, maxY)
    MYSIMPLE_PROP(int,   paramY)

    Q_PROPERTY( BBBDataSource* datasource  MEMBER ds_ WRITE setDatasource  NOTIFY datasourceChanged )
    Q_PROPERTY( int  regimX  MEMBER regimX_ WRITE setRegimX  NOTIFY regimXChanged )

public:
    explicit XYProvider(QObject *parent = nullptr);
    ~XYProvider();

    void setDatasource(BBBDataSource* ds);
    void setRegimX(int r);
    Q_INVOKABLE void updateSeries() { last_tick_=0; Q_EMIT dataChanged(); }

signals:
    void datasourceChanged();
    void dataChanged();
    void regimXChanged();

private:
    QPointF generate_point(const BBBDataSource::data_type& d);

public slots:
    void updateFromQML( QtCharts::QAbstractSeries *s
//                       , QtCharts::QAbstractAxis* xa
//                       , QtCharts::QAbstractAxis* ya
                        );


private:
    BBBDataSource*      ds_;
    int                 regimX_;
    uint                last_tick_;
};

#endif // XYPROVIDER_H
