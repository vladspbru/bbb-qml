/*************************************************************************
 *
 * Copyright (c) 2012 Digia Plc.
 * All rights reserved.
 *
 * See the LICENSE.txt file shipped along with this file for the license.
 *
 *************************************************************************/
#include <QtWidgets/QApplication>
//#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDir>
//#include "bbbdevice.h"
#include "bbb_plugin.h"

const char uri[]= "org.gt.vxcomponents";

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

//    qmlRegisterType<BBBDevice>(uri, 1, 0, "BBBDevice");
    BBBPlugin plg;
    plg.registerTypes(uri);

    QQmlApplicationEngine engine;
    engine.addImportPath( "qrc:/files/" );
//    engine.addImportPath( app.applicationDirPath() + "/plugins/" );
    engine.load(QUrl(QStringLiteral("qrc:/files/main.qml") ));
    return app.exec();
}
