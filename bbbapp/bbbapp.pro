TEMPLATE = app
TARGET    = bbb-app

QT     += qml quick charts widgets
CONFIG += c++11

include(../bbbplugin/bbbplug.pri)
INCLUDEPATH += ../bbbplugin

RESOURCES += \
	application.qrc

SOURCES   += \
	bbbmain.cpp

DESTDIR   = ../

