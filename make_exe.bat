@set QTROOT=D:\Qt\Qt5.11.1\5.11.1\mingw53_32
@set PATH=D:\Qt\Qt5.11.1\Tools\mingw530_32\bin;%QTROOT%\bin;%PATH%


@set BDIR=build.win32
if exist %BDIR%\ rd /s /q %BDIR%\
mkdir %BDIR%
@cd  %BDIR%



@set LOG=..\build-win32.log 
qmake  ..\bbb-qml.pro  >%LOG%
mingw32-make.exe -j4  >>%LOG%


@set APP=bbb-app.exe
mkdir _deploy
cd _deploy
copy ..\%APP% .\%APP%
windeployqt.exe --release --no-translations --no-system-d3d-compiler --compiler-runtime --verbose 7  --qmldir %QTROOT%\qml  %APP%  >>..\%LOG%
@xcopy  %QTROOT%\qml\QtCharts .\QtCharts /E /I /S >>..\%LOG%



