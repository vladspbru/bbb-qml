import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Window 2.11
import org.gt.vxcomponents 1.1

Page {
    id : pagePlot
    width: 800
    height: 600

    header: Label {
        text: qsTr("Потоки")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Flickable {
        anchors.fill: parent
        contentHeight: grid.height
        contentWidth: grid.width

        focus: true
        Keys.onUpPressed: scrollBar.decrease()
        Keys.onDownPressed: scrollBar.increase()
        ScrollBar.vertical: ScrollBar { id: scrollBar }

        Flow {
            id: grid
            width: pagePlot.width
            Repeater {
                id: bbbsRepeater
                model: BBBs.count
                BBBPlot {
                    addr : index
                }
            }
        }
    }

    /*
    ListView {
        id: lisView
        anchors.fill: parent
        focus: true
        model: BBBs.count//[1,3,3,3,3,3,3,3,3,31]
        delegate: BBBPlot {
            addr   : index//lisView.model[index]
            width: lisView.width

            MouseArea {
                anchors.fill: parent
                onClicked:{
                    lisView.focus = true;
                    lisView.currentIndex = index;
                }
            }
        }

        highlight: Component {
            Rectangle {
                width: lisView.width
                color: "#f3f7bc"
            }
        }
        Keys.onUpPressed: listView.decrementCurrentIndex()
        Keys.onDownPressed: listView.incrementCurrentIndex()
    }
    */

}
