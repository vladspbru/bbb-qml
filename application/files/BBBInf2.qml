import QtQuick 2.11
import org.gt.vxcomponents 1.1

Item {
    id : root
    width: 250
    height: 100
    clip: true

    Rectangle {
        id: rect
        anchors { fill: parent; margins: 8}
        clip: true

        Column{
            Text {
                text: "ip= "+bbb.ipaddr
                color: ("192.168.4."+(bbb.addr+200))==bbb.ipaddr ? "green" : "red"
            }
            Text {
                text: "tm= "+bbb.ms/1000
            }
            Text {
                text: "tick= "+bbb.count
            }
        }
    }
}
