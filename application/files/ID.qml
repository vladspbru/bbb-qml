import QtQuick 2.5


Rectangle {
    id: root

    property alias text: label.text
    property bool  active: false
    signal clicked

    width: 40
    height: width
    color: "lightgrey"
    border.color: color
    border.width: 1
    radius: width*0.5

    Text {
        id: label
        anchors.centerIn: parent
        text: "ID"
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            root.clicked()
        }
    }

    onActiveChanged: active ? color="lightgreen" : color="lightgrey"
}
