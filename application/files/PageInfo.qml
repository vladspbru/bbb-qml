import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Window 2.11
import org.gt.vxcomponents 1.1

Page {
    id : pageInfo
    height: 600
    width: 800

    header: Label {
        text: qsTr("Контроллеры")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Flickable {
        anchors.fill: parent
        contentHeight: grid.height
        contentWidth: grid.width

        focus: true
        Keys.onUpPressed: scrollBar.decrease()
        Keys.onDownPressed: scrollBar.increase()
        ScrollBar.vertical: ScrollBar { id: scrollBar }

        Flow {
            id: grid
            width: pageInfo.width
            spacing: 4
            Repeater {
                id: bbbsRepeater
                model: BBBs.count
                BBBInfo {
                    addr : index
                }
            }
        }
    }
}
