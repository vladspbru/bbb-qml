import QtQuick 2.5


Rectangle {
    id: root

    property alias text: label.text
    property color animcolor: "yellow"

    width: label.width + 8
    height: label.height + 2
//    color: "lightsteelblue"
//    border.color: "slategrey"

    Text {
        id: label
        anchors.centerIn: parent
        text: "ooo"
    }

    SequentialAnimation on color {
        id: anim
        loops: 1
        PropertyAnimation { to: animcolor }
        PropertyAnimation { to: "transparent" }
    }

    onTextChanged: anim.start()
}
