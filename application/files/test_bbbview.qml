import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Window 2.11
import org.gt.vxcomponents 1.1

ApplicationWindow {
    title: qsTr("Test BBB view")
    visible: true
    width: Screen.width*0.5
    height: Screen.height*0.5
    id: app

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                checkable: true
                checked: true
                text: qsTr("&Network")
                onTriggered: {
                    checked: !checked
                    GtNet.enabled = checked
                }
            }
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

    Connections {
        id: gtlan
        target: GtNet
        onEnabledChanged: {
            console.log("Value changed")
        }
    }

    Flow {
        anchors.fill: parent
        spacing: 4
        Repeater {
            id: bbbsRepeater
            model: 4
            Column{
                BBBInfo {
                    ad   : index
                    name : "A-" + ad
                    capture: 10
                }
                BBBPlot {
                    width: 300
                }
            }
        }
    }


}
