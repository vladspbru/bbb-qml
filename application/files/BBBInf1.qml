import QtQuick 2.11
import org.gt.vxcomponents 1.1

Item {
    id : root
    width: 250
    height: 100
    clip: true

//    property BBBDevice bbb: null

    function rejim() {
        var r="";
        switch(bbb.conf & 0x03){
        case 0: r="***";  break;
        case 1: r="1й";   break;
        case 2: r="2й";   break;
        case 3: r="НТП";  break;
        }
        if(bbb.conf & 0x08)
            r = r+"+8"
        if(bbb.conf & 0x04)
            r = r+"+Осц"
        return r;
    }

    function sigs() {
        if( bbb.sigs == 0 )
            return "-"
        var r="";
        if(bbb.sigs & 0x01)
            r = r+" РРС"
        if(bbb.sigs & 0x02)
            r = r+" РЦ"
        if(bbb.sigs & 0x04)
            r = r+" ПредРЦ"
        return r;
    }

    function out() {
        var _L="";
        switch(bbb.out){
        default: _L=bbb.out;   break;
        case 0:  _L="OT";   break;
        case 32: _L=" - ";   break;
        case 1:  _L="T1";   break;
        case 2:  _L="T2";   break;
        case 4:  _L="T3";   break;
        case 8:  _L="T4";   break;
        case 3:  _L="T0.5";   break;
        case 5:  _L="T1.5";   break;
        case 6:  _L="T2.5";   break;
        case 7:  _L="T3.5";   break;
        }
        return _L;
    }

    Rectangle {
        id: rect
        anchors { fill: parent; margins: 8}
        clip: true
        border.color: "lightgrey"
        color : bbb.alive ? "white" : "lightgrey"

        Column{
            id: column
            visible: bbb.alive
            anchors { fill: parent; margins: 4 }
            Row {
                Text {
                    text: bbb.name
                    font.weight: Font.Bold
                    width : column.width/3
                }
                Lamp {
                    text: root.rejim()
                    animcolor: "red"
                }
            }
            Row {
                    Lamp {
                        text: root.sigs()
                        width : column.width/2
                    }
                    Lamp {
                        text: root.out()
                        animcolor: "lightsteelblue"
                        width : column.width/2
                    }
            }
            Row {
                Lamp {
                    text: "Vz= " + bbb.vz //+ " V= " + bbb.v
                    animcolor: "green"
                    width : column.width/2
                }
                Rectangle{
                    width : label.width
                    height: label.height + 2
                    Text {
                        id: label
                        text: "F= " + bbb.f + " Hz"
                        color: bbb.f != 0 ? "black": "red"
                    }
                }

            }
        }
    }
}


/*##^## Designer {
    D{i:0;height:100;width:250}D{i:9;invisible:true}
}
 ##^##*/
