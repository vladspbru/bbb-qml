import QtQuick 2.9
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import QtCharts 2.1

ApplicationWindow {
    id: root
    visible: true
    width: 640
    height: 480

    property int numpoints: 628;
    property double step: 0.01;

    Timer {
        id: timer;
        interval: 1;
        repeat: true;
        onTriggered: {
            draw( xAxis, yAxis, lineSeries, lineSeries.at(0).y );
            if( xAxis.max - (-xAxis.categoriesLabels[xAxis.categoriesLabels.length -1]) > 1 ){  // --> Watch out for negatives
                xAxis.append(("-"+xAxis.max.toFixed(0)), xAxis.max);
                xAxis.remove(xAxis.categoriesLabels[0]);
            }
        }
    }

    function draw(xax, yax, lineSeries, dataPoint){
        lineSeries.remove(0);
        xax.min = xax.min + step;
        xax.max = xax.max + step;

        var x = lineSeries.at(lineSeries.count-1).x + step ;
        lineSeries.append( x, dataPoint );
        if(dataPoint > yax.max){
            yax.max = dataPoint +1;
        } else if(dataPoint < yax.min){
            yax.min = dataPoint -1;
        }
    }


    ColumnLayout {
        anchors.rightMargin: 20
        anchors.leftMargin: 20
        anchors.bottomMargin: 20
        anchors.topMargin: 20
        anchors.fill: parent

        ChartView {
            id: chartView
            width: 631
            height: 400
            Layout.fillHeight: true
            Layout.fillWidth: true
            title: "Sliding Grid"
            antialiasing: true
            legend {
                alignment: Qt.AlignBottom
            }

            CategoryAxis {
                id: xAxis
                reverse: true // ----> This is the important property!
                min: 0
                max: numpoints * step + (numpoints*step)/6;

                labelsPosition: CategoryAxis.AxisLabelsPositionOnValue;
                Component.onCompleted: {
                    for(var i=0; i<max+1; i++){
                        xAxis.append( ("-"+i), i );
                    }
                }
            }

            ValueAxis {
                id: yAxis
                min: 0
                max: 10

                tickCount: 5
                minorTickCount: 5
            }

            LineSeries {
                id: lineSeries
                name: "Line Series"
                axisX: xAxis
                axisY: yAxis

                Component.onCompleted: {
                    for (var i = 0; i < numpoints; i++) {
                        lineSeries.append(i*step, Math.sin(i*step) * 3 + 5);
                    }
                    timer.start();
                }
            }
        }
    }
}
