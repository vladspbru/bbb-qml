import QtQuick 2.11
import QtCharts 2.2
import QtQuick.Controls 2.2
import org.gt.vxcomponents 1.1

Item {
    id : root
    width: 800;
    height: 350
    clip: true

    property BBBDataSource ds: BBBs.datasource(addr)
    property bool viewPlus: menu_item_sview.checked
    property int  regimx: regimXButton.currentSelection
    property bool openGL: menu_item_opengl.checked
    property bool pause: menu_item_pause.checked

    ChartView {
        id: chartView
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        height: parent.height-sb.height
        //animationOptions: ChartView.NoAnimation //ChartView.SeriesAnimations;
        title:  provV.datasource.device.name
        antialiasing: !root.openGL
        margins {
            top:0
            bottom: 0
            left: 0
            right: 0
        }

        legend {
            alignment: Qt.AlignBottom
            visible: root.viewPlus
        }

        ValueAxis {
            id: axisX
            property real minValue: provV.minX
            property real maxValue: provV.maxX
            property real range: 100

            min: minValue + sb.position * (maxValue - minValue - range)
            max: minValue + sb.position * (maxValue - minValue - range) + range
            //            min: provV.maxX-50
            //            max: provV.maxX
            //            tickCount: 20
            labelFormat: "%.0f"
            labelsVisible: root.viewPlus
            labelsColor: "gray"
            labelsFont.pointSize: 5
        }

        LineSeries {
            id: seriesRC
            name: "РЦ"
            useOpenGL: root.openGL
            axisX: axisX
            axisY: ValueAxis {
                min: 0
                max: 1
                visible: root.viewPlus
                color: seriesRC.color
                tickCount: 2
                labelsColor: color
                labelFormat: "%.0f"
                labelsFont.pointSize: 5
            }
        }

        LineSeries {
            id: seriesTP
            name: "TП"
            useOpenGL: root.openGL
            pointLabelsVisible : false
            pointLabelsColor: color
            pointLabelsFormat: "{@yPoint}"

            axisX: axisX
            axisY: ValueAxis {
                min: -1
                max: 4
                visible: root.viewPlus
                color: seriesTP.color
                tickCount: 6
                minorTickCount: 1
                labelsColor: color
                labelFormat: "%.0f"
                labelsFont.pointSize: 5
                //                titleFont.pointSize: 5
                //                titleVisible: true
                //                titleText: "ступени"

            }
        }

        LineSeries {
            id: seriesV
            name: "V"
            useOpenGL: root.openGL
            axisX: axisX
            axisYRight: ValueAxis {
                id: axisY_V
                min: -1
                max: 35
                color: seriesV.color
                labelFormat: "%.0f"
                labelsColor: color
            }
        }

        LineSeries {
            id: seriesVz
            name: "Vз"
            useOpenGL: root.openGL
            width: 1
            style: Qt.DashLine
            color: "red"
            axisX: axisX
            axisYRight: axisY_V
        }

        LineSeries {
            id: seriesF
            name: "F"
            useOpenGL: root.openGL
            width: 1
            color: "gray"
            visible: root.viewPlus
            axisX: axisX
            axisYRight: ValueAxis {
                min: provF.minY /*- Math.abs(provF.minY)*0.1*/
                max: provF.maxY+1 /*+ Math.abs(provF.maxY)*0.1*/
                visible: root.viewPlus
                color: seriesF.color
                labelsColor: color
                labelsFont.pointSize: 5
                //                titleFont.pointSize: 5
                //                titleVisible: true
                //                titleText: "Hz"
            }
        }

        LineSeries {
            id: seriesA
            name: "A"
            useOpenGL: root.openGL
            width:1
            color: "lightsteelblue"
            visible: root.viewPlus
            axisX: axisX
            axisYRight: ValueAxis {
                min: provA.minY-1
                max: provA.maxY+1
                visible: root.viewPlus
                color: seriesA.color
                labelsColor: color
                labelsFont.pointSize: 5
                //                labelFormat: "%.0f"
            }
        }


    }

    Slider {
        id: sb
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        height: 30
        value: axisX.range
    }


    XYProvider {
        id: provRC
        datasource: ds
        paramY: 0
        regimX: root.regimx
        onDataChanged: {
            if( !root.pause )
                provRC.updateFromQML(seriesRC)
        }
    }

    XYProvider {
        id: provTP
        datasource: ds
        paramY: 1
        regimX: root.regimx
        onDataChanged: {
            if( !root.pause )
                provTP.updateFromQML(seriesTP)
        }
    }

    XYProvider {
        id: provV
        datasource: ds
        paramY: 2
        regimX: root.regimx
        onDataChanged: {
            if( !root.pause )
                provV.updateFromQML(seriesV)
        }
    }

    XYProvider {
        id: provVz
        datasource: ds
        paramY: 3
        regimX: root.regimx
        onDataChanged: {
            if( !root.pause )
                provVz.updateFromQML(seriesVz)
        }
    }

    XYProvider {
        id: provF
        datasource: ds
        paramY: 4
        regimX: root.regimx
        onDataChanged: {
            if( !root.pause )
                provF.updateFromQML(seriesF)
        }
    }

    XYProvider {
        id: provA
        datasource: ds
        paramY: 5
        regimX: root.regimx
        onDataChanged: {
            if( !root.pause )
                provA.updateFromQML(seriesA)
        }
    }


    Rectangle{
        id: rectang
        color: "black"
        opacity: 0.4
        visible: false
    }

    MouseArea{
        anchors.fill: chartView
        hoverEnabled: true
        acceptedButtons: Qt.AllButtons

        onPressed: { preventStealing = true; rectang.x = mouseX; rectang.y = mouseY; rectang.visible = true}
        onMouseXChanged: {rectang.width = mouseX - rectang.x}
        onMouseYChanged: {rectang.height = mouseY - rectang.y}
        onReleased: {
            if(rectang.width<0 || rectang.height<0){
                chartView.zoomReset();
            }
            else{
                //                chartView.zoomIn();
                chartView.zoomIn(Qt.rect(rectang.x, rectang.y, rectang.width, rectang.height));
            }
            rectang.visible = false
            preventStealing = false
        }
    }
}
