import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Window 2.11
import org.gt.vxcomponents 1.1

ApplicationWindow {
    id: appwindow
    title: qsTr("BBB realtime view")
    visible: true
    width: Screen.width*0.7
    height: Screen.height*0.7

    Component.onCompleted: {
        BBBs.count=29
    }


    Timer {
        id: refreshTimer
        interval: 1 / 2 * 1000 // 2 Hz
        running: false
        repeat: true
        onTriggered: {
            for(var i=0; i<BBBs.count; i++) {
                var bbb = BBBs.at(i)
                bbb.sendQuery("192.168.4.255");
            }
        }
    }

    menuBar: MenuBar {
        Menu {
            title: qsTr("&Application")
            MenuItem {
                checkable: true
                checked: true
                text: qsTr("&Network")
                onTriggered: {
                    checked: !checked
                    GtNet.enabled = checked
                }
            }
            MenuItem {
                checkable: true
                checked: false
                text: qsTr("&Autocall")
                onTriggered: {
                    checked: !checked
                    refreshTimer.running = checked
                }
            }
            MenuItem {
                text: qsTr("&Save data")
                onTriggered: {
                    for(var i=0; i<BBBs.count; i++) {
                        var ds = BBBs.datasource(i)
                        ds.save_captured();
                    }
                }
            }
            MenuItem {
                text: qsTr("Clear data")
                onTriggered: {
                    for(var i=0; i<BBBs.count; i++) {
                        var ds = BBBs.datasource(i)
                        ds.clear();
                    }
                }
            }
            MenuItem {
                text: qsTr("E&xit")
                onTriggered: Qt.quit();
            }
        }
        Menu {
            title: qsTr("&Plot")
            MenuItem {
                id: menu_item_pause
                checkable: true
                checked: false
                text: qsTr("Pause")
            }
            MultiMenuItem {
                id: regimXButton
                label: "X &-> "
                items: ["counter", "syntetic", "seconds"]
                currentSelection: 0
                onSelectionChanged: console.log(items[currentSelection])
            }
            MenuItem {
                id: menu_item_sview
                checkable: true
                checked: false
                text: qsTr("&View ++")
            }
            MenuItem {
                id: menu_item_opengl
                checkable: true
                checked: false
                text: qsTr("OpenGL")
            }
        }
    }

    Connections {
        id: ars
        target: BBBs
        onCountChanged: {
            console.log("BBBs count = " + BBBs.count)
        }
    }

    Connections {
        id: gtlan
        target: GtNet
        onEnabledChanged: {
            console.log("GtNet changed")
        }
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        PageInfo{
        }

        PagePlot {
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Панель")
        }
        TabButton {
            text: qsTr("Графики")
        }
    }
}
