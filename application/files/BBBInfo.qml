import QtQuick 2.11
import QtQuick.Controls 2.4
import org.gt.vxcomponents 1.1

Item {
    id : root
    width: 250
    height: 100
    clip: true

    property int  addr:  -1
    property BBBDevice bbb: null

    onAddrChanged: {
        root.bbb=BBBs.at(addr)
        console.log( "bbb addr changed to " + addr )
    }

    SwipeView{
        id: swipeView
        anchors.fill: parent
        BBBInf1 {
        }
        BBBInf2 {
        }
    }


    ID {
        id: idObj
        text: bbb.addr
        active: bbb.alive
        onClicked: {
            var addr = bbb.addr==0 ? "192.168.1.255" : "192.168.4.255";
            bbb.sendQuery(addr);
            console.log( "query bbb-" + bbb.addr + " on " + addr );
        }
    }

    states: [
        State {
            name: "alive"
            PropertyChanges { target: idObj; x: root.width - idObj.width}
        },
        State {
            name: "dead"
            PropertyChanges { target: idObj; x: 0 }
        }
    ]

    transitions: [
        Transition {
            NumberAnimation {
                duration: 200;
                properties: "x"
            }
        }
    ]

    Connections{
        target: bbb
        onAliveChanged: root.state = bbb.alive ?  "alive" : "dead"
    }

}
