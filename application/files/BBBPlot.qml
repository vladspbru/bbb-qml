import QtQuick 2.11
import org.gt.vxcomponents 1.1

Item {
    id : root
    width: 450;
    height: 300

    property int  addr:  -1
    visible: chartView.ds.device.alive && chartView.ds.count>2

    BBBChartView {
        id: chartView
        anchors.fill: parent
    }
}
