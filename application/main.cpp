/*************************************************************************
 *
 * Copyright (c) 2012 Digia Plc.
 * All rights reserved.
 *
 * See the LICENSE.txt file shipped along with this file for the license.
 *
 *************************************************************************/
#include <QtWidgets/QApplication>
//#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDir>
#include <QQmlExtensionPlugin>
#include <QPluginLoader>
//#include <QQuickView>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

/*
    QDir pluginsDir(app.applicationDirPath() + "/plugins");
    foreach (const QString &fileName, pluginsDir.entryList(QDir::Files)) {
        QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
        QQmlExtensionPlugin *plugin = qobject_cast<QQmlExtensionPlugin *>(loader.instance());
        if (plugin){
            plugin->registerTypes("org.gt.vxcomponents");
        }
    }
*/
    QQmlApplicationEngine engine;
    engine.addImportPath( "qrc:/files/" );
    engine.addImportPath( app.applicationDirPath() + "/plugins/" );
    engine.load(QUrl(QStringLiteral("qrc:/files/main.qml") ));


//    QQuickView viewer;
//    QObject::connect(viewer.engine(), &QQmlEngine::quit, &viewer, &QWindow::close);
//    viewer.setTitle(QStringLiteral("QML BBB scope"));
//    viewer.setSource(QUrl( QStringLiteral("qrc:/files/main.qml") ));
//    viewer.setResizeMode(QQuickView::SizeRootObjectToView);
//    viewer.setColor(QColor("#404040"));
//    viewer.show();

    return app.exec();
}
