TEMPLATE = app

QT       += qml quick widgets
CONFIG += c++11

RESOURCES = application.qrc
SOURCES   = main.cpp

TARGET    = bbb-quick
DESTDIR   = ../


#win32 {
#        QMAKE_LFLAGS += -static-libgcc
#        QMAKE_LFLAGS += -static
#}

#OTHER_FILES = application/files/main.qml

# Additional import path used to resolve QML modules in Qt Creator's code model
#QML_IMPORT_PATH = ../plugins/
#QML2_IMPORT_PATH = ../plugins/

# Default rules for deployment.
#include(deployment.pri)
